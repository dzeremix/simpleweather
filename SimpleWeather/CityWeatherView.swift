//
//  CItyWeatherView.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit
import CoreData

class CityWeatherView: UIView {

    let keywords = ["sunny", "storm", "Drizzle", "raining", "Winter", "Fog", "Cloudy", "weather", "Sleep"]
    let colors = [UIColor(red: 255.0/255.0, green: 210.0/255.0, blue: 64.0/255.0, alpha: 1.0),
                  UIColor(red: 63.0/255.0, green: 45.0/255.0, blue: 194.0/255.0, alpha: 1.0),
                  UIColor(red: 14.0/255.0, green: 214.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                  UIColor(red: 36.0/255.0, green: 175.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                  UIColor(red: 197.0/255.0, green: 236.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                  UIColor(red: 182.0/255.0, green: 176.0/255.0, blue: 191.0/255.0, alpha: 1.0),
                  UIColor(red: 57.0/255.0, green: 119.0/255.0, blue: 191.0/255.0, alpha: 1.0),
                  UIColor(red: 235.0/255.0, green: 81.0/255.0, blue: 110.0/255.0, alpha: 1.0),
                  UIColor(red: 239.0/255.0, green: 144.0/255.0, blue: 255.0/255.0, alpha: 1.0)]
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var detailWeatherLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var fiveDayForecastCollectionView: UICollectionView!
    
    var cityData: [String: Any]? {
        didSet {
            var unit: String = "C"
            if let unitSystem = UserDefaults.standard.object(forKey: "unit-system") as? String {
                switch unitSystem {
                case "metric":
                    unit = "C"
                case "imperial":
                    unit = "F"
                default:
                    unit = "C"
                }
            }
            
            guard let main = cityData?["main"] as? NSDictionary else { return }
            guard let temperature = main["temp"] as? Int else { return }
            guard let humidity = main["humidity"] as? Int else { return }
            
            self.temperatureLabel.text = "\(temperature)º\(unit)"
            
            var precipitation = 0
            if let precipitationData = cityData?["precipitation"] as? NSDictionary {
                guard let precipitationValue = precipitationData["value"] as? Int else { return }
                precipitation = precipitationValue
            }
            
            self.detailWeatherLabel.text = "Humidity: \(humidity)%, Rain: \(precipitation)mm"
            
            guard let wind = cityData?["wind"] as? NSDictionary else { return }
            let speed = wind["speed"] as? Int ?? 0
            let deg = wind["deg"] as? Float ?? 0.0
            
            self.windLabel.text = "Wind: \(speed) km/h, \(self.string(from: deg))"
            
            guard let weather = cityData?["weather"] as? NSArray else { return }
            guard let weatherData = weather.firstObject as? NSDictionary else { return }
            let icon = weatherData["icon"] as? String ?? "na"
            let id = weatherData["id"] as? Int ?? 0
            
            if icon == "01n" {
                self.weatherText(for: -id)
            } else {
                self.weatherText(for: id)
            }
            self.weatherLabel.attributedText = self.colorSpecificWord()
            
            if let weatherImage = UIImage(named: icon) {
                self.weatherImageView.image = weatherImage
            } else {
                let dayIcon = icon.replacingOccurrences(of: "n", with: "d")
                
                if let weatherImage = UIImage(named: dayIcon) {
                    self.weatherImageView.image = weatherImage
                } else {
                    self.weatherImageView.image = UIImage(named: "na")
                }
            }
        }
    }
    
    private func string(from degree: Float) -> String {
        let degreeSymbolNumber: Int = Int(Int(degree)/45)
        
        switch degreeSymbolNumber {
        case 0:
            return "N"
        case 1:
            return "NE"
        case 2:
            return "E"
        case 3:
            return "SE"
        case 4:
            return "S"
        case 5:
            return "SW"
        case 6:
            return "W"
        case 7:
            return "NW"
            
        default:
            return "N"
        }
    }
    
    func weatherText(for id: Int) {
        if id == 800 {
            descriptionLabel.text = "Keep smiling!"
            weatherLabel.text = "It is sunny day"
        } else if id == -800 {
            descriptionLabel.text = "Beautiful night!"
            weatherLabel.text = "Sleep well"
        } else {
            let weatherClassId = id/100
            
            switch weatherClassId {
            case 2:
                descriptionLabel.text = "Watch out for lightning!"
                weatherLabel.text = "The storm is coming"
            case 3:
                descriptionLabel.text = "Small drops from the sky."
                weatherLabel.text = "Drizzle behind the window"
            case 5:
                descriptionLabel.text = "Take an umbrella!"
                weatherLabel.text = "It's raining outside"
            case 6:
                descriptionLabel.text = "Dress warmly!"
                weatherLabel.text = "Winter is coming"
            case 7:
                descriptionLabel.text = "Be attentive!"
                weatherLabel.text = "Fog covered the world"
            case 8:
                descriptionLabel.text = "Nothing special..."
                weatherLabel.text = "Cloudy day"
            case 9:
                descriptionLabel.text = "Prepare for anything!"
                weatherLabel.text = "The weather is crazy"
                
            default:
                descriptionLabel.text = "Check the window."
                weatherLabel.text = "I do not know what's going on"

            }
        }
    }

    private func colorSpecificWord() -> NSMutableAttributedString? {
        var range = NSRange()
        var color = UIColor.black
        
        if let weatherString = weatherLabel.text {
            let attributedString = NSMutableAttributedString(string: weatherString)
            
            for (index, word) in self.keywords.enumerated() {
                if weatherString.contains(word) {
                    range = (weatherString as NSString).range(of: word)
                    color = colors[index]
                }
            }
            
            attributedString.addAttribute(NSForegroundColorAttributeName, value: color, range: range)
            
            return attributedString
        }
        
        return nil
    }
}
