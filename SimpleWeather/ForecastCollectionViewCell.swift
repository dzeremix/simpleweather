//
//  ForecastCollectionViewCell.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    var forecastData: NSDictionary? {
        didSet {
            var unit: String = "C"
            if let unitSystem = UserDefaults.standard.object(forKey: "unit-system") as? String {
                switch unitSystem {
                case "metric":
                    unit = "C"
                case "imperial":
                    unit = "F"
                default:
                    unit = "C"
                }
            }
            
            guard let main = forecastData?["main"] as? NSDictionary else { return }
            guard let temperature = main["temp"] as? Int else { return }
            
            self.weatherLabel.text = "\(temperature)º\(unit)"
            
            guard let weather = forecastData?["weather"] as? NSArray else { return }
            guard let weatherData = weather.firstObject as? NSDictionary else { return }
            guard let icon = weatherData["icon"] as? String else { return }
            
            if let weatherImage = UIImage(named: icon) {
                self.weatherImageView.image = weatherImage
            } else {
                let dayIcon = icon.replacingOccurrences(of: "n", with: "d")
                
                if let weatherImage = UIImage(named: dayIcon) {
                    self.weatherImageView.image = weatherImage
                } else {
                    self.weatherImageView.image = UIImage(named: "na")
                }
            }
            
            guard let dateTimestamp = forecastData?["dt"] as? Int else { return }
            let date = Date(timeIntervalSince1970: TimeInterval(dateTimestamp))
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "dd.MM, HH:mm"
            dayLabel.text = dateFormatter.string(from: date)
        }
    }
}
