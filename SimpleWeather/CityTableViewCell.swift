//
//  CityTableViewCell.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit
import CoreData

class CityTableViewCell: UITableViewCell {
    
    var rawData: [String: Any]?
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    @IBOutlet weak var gpsImageView: UIImageView!
    
    var cityData: NSManagedObject? {
        didSet {
            var unit: String = "C"
            if let unitSystem = UserDefaults.standard.object(forKey: "unit-system") as? String {
                switch unitSystem {
                case "metric":
                    unit = "C"
                case "imperial":
                    unit = "F"
                default:
                    unit = "C"
                }
            }
            
            guard let cityName = cityData?.value(forKeyPath: "name") as? String else { return }
            guard let latitude = cityData?.value(forKeyPath: "latitude") as? Float else { return }
            guard let longitude = cityData?.value(forKeyPath: "longitude") as? Float else { return }
            
            self.cityLabel.text = cityName
            
            APIManager.todayForecast(latitude: latitude, longitude: longitude, completion: { response, error in
                if error != nil {
                    print(error?.localizedDescription ?? "Error occured")
                }
                
                self.rawData = response
                
                guard let main = response?["main"] as? NSDictionary else { return }
                guard let temperature = main["temp"] as? Int else { return }
                
                self.temperatureLabel.text = "\(temperature)º\(unit)"
                
                guard let weather = response?["weather"] as? NSArray else { return }
                guard let weatherData = weather.firstObject as? NSDictionary else { return }
                guard let icon = weatherData["icon"] as? String else { return }
                
                if let weatherImage = UIImage(named: icon) {
                    self.weatherImageView.image = weatherImage
                } else {
                    let dayIcon = icon.replacingOccurrences(of: "n", with: "d")
                    
                    if let weatherImage = UIImage(named: dayIcon) {
                        self.weatherImageView.image = weatherImage
                    } else {
                        self.weatherImageView.image = UIImage(named: "na")
                    }
                }
            })
        }
    }
}
