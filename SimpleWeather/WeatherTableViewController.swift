//
//  WeatherTableViewController.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class WeatherTableViewController: UITableViewController {

    var cities: [NSManagedObject] = []
    var detailViewController: CityWeatherViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Prepare User Interface
        let settingsButton = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(settingsButtonPressed(_:)))
        
        self.navigationItem.leftBarButtonItem = settingsButton
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:)))
        
        self.toolbarItems = [flexibleSpace, addButton]
        
        //Load data
        var managedContext: NSManagedObjectContext
        if #available(iOS 10.0, *) {
            managedContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
        } else {
            managedContext = CoreDataManager.sharedInstance.managedObjectContext
        }
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "City")
        
        do {
            cities = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        //Present first view controller
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? CityWeatherViewController
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CityTableViewCell {
                    self.detailViewController?.cityData = cell.rawData
                    self.detailViewController?.cityName = cell.cityLabel.text
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addSegue" {
            let controller = (segue.destination as! UINavigationController).topViewController as! AddCityViewController
            controller.delegate = self
        }
        
        if segue.identifier == "settingsSegue" {
            let controller = (segue.destination as! UINavigationController).topViewController as! SettingsViewController
            controller.delegate = self
        }
        
        if segue.identifier == "citySegue" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let cityCell = self.tableView.cellForRow(at: indexPath) as! CityTableViewCell
                let controller = (segue.destination as! UINavigationController).topViewController as! CityWeatherViewController
                
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                controller.cityData = cityCell.rawData
                controller.cityName = cityCell.cityLabel.text
            }
        }
    }
    
    // MARK: - Actions
    
    func addButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "addSegue", sender: self)
    }
    
    func settingsButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "settingsSegue", sender: self)
    }
    
    @IBAction func refreshControlAction(_ sender: UIRefreshControl) {
        self.tableView.reloadData()
        sender.endRefreshing()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! CityTableViewCell
        cell.cityData = cities[indexPath.row]

        return cell
    }

    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            var managedContext: NSManagedObjectContext
            if #available(iOS 10.0, *) {
                managedContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
            } else {
                managedContext = CoreDataManager.sharedInstance.managedObjectContext
            }
            managedContext.delete(self.cities[indexPath.row])
            
            self.cities.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

extension WeatherTableViewController: AddCityViewControllerDelegate {
    func doneButtonWasPressed(data: CLPlacemark) {
        
        for city in cities {
            guard let cityName = city.value(forKeyPath: "name") as? String else { return }
            
            if cityName == data.locality {
                return
            }
        }
        
        var managedContext: NSManagedObjectContext
        if #available(iOS 10.0, *) {
            managedContext = CoreDataManager.sharedInstance.persistentContainer.viewContext
        } else {
            managedContext = CoreDataManager.sharedInstance.managedObjectContext
        }
        let entity = NSEntityDescription.entity(forEntityName: "City", in: managedContext)!
        let city = NSManagedObject(entity: entity, insertInto: managedContext)
        
        city.setValue(data.locality, forKeyPath: "name")
        city.setValue(data.location?.coordinate.latitude, forKey: "latitude")
        city.setValue(data.location?.coordinate.longitude, forKey: "longitude")
        
        do {
            try managedContext.save()
            cities.append(city)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        self.tableView.reloadData()
    }
}

extension WeatherTableViewController: SettingsViewControllerDelegate {
    func settingsDismissed() {
        self.tableView.reloadData()
    }
}
