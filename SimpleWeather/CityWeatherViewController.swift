//
//  CityWeatherViewController.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit
import CoreData

class CityWeatherViewController: UIViewController {

    @IBOutlet weak var forecastCollectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var heightCollectionViewConstraint: NSLayoutConstraint!
    
    @IBOutlet var weatherInfoCenterXConstraint: [NSLayoutConstraint]!
    
    var forecastData: NSArray = []
    
    var cityName: String? {
        didSet {
            self.title = cityName
        }
    }
    
    var cityData: [String: Any]? {
        didSet {
            guard let cityView = self.view as? CityWeatherView else { return }
            cityView.cityData = self.cityData
            
            guard let coord = cityData?["coord"] as? NSDictionary else { return }
            guard let latitude = coord["lat"] as? Float else { return }
            guard let longitude = coord["lon"] as? Float else { return }
            
            APIManager.fiveDayForecast(latitude: latitude, longitude: longitude, completion: { response, error in
                if error != nil {
                    print(error?.localizedDescription ?? "Error occured")
                }
                
                guard let list = response?["list"] as? NSArray else { return }
                self.forecastData = list
                
                guard let cityView = self.view as? CityWeatherView else { return }
                cityView.fiveDayForecastCollectionView.reloadData()
                
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellHeight = UIScreen.main.bounds.size.height * heightCollectionViewConstraint.multiplier
        let cellWidth = cellHeight * 0.71258
        forecastCollectionViewLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        
        let size = self.view.bounds.size
        self.updateView(with: size)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let cellHeight = size.height * heightCollectionViewConstraint.multiplier
        let cellWidth = cellHeight * 0.71258
        forecastCollectionViewLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        
        self.updateView(with: size)
    }
    
    func updateView(with size: CGSize) {
        if UIDevice.current.userInterfaceIdiom == .pad { return }
        
        if UIDevice.current.orientation.isLandscape {
            let constantChange = (size.width/2) - (size.height * 0.26)
            for constraint in weatherInfoCenterXConstraint {
                constraint.constant = constantChange
            }
            
            self.view.layoutIfNeeded()
        } else {
            for constraint in weatherInfoCenterXConstraint {
                constraint.constant = 0
            }
            
            self.view.layoutIfNeeded()
        }
    }
}

extension CityWeatherViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecastData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "forecastCell", for: indexPath) as! ForecastCollectionViewCell
        guard let forecast = forecastData[indexPath.row] as? NSDictionary else { return cell }
        cell.forecastData = forecast
        
        return cell
    }
    
    
}
