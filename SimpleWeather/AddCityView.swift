//
//  AddCityView.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit
import MapKit

class AddCityView: UIView {

    @IBOutlet weak var mapView: MKMapView!

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    public var pinPoint: CGPoint? {
        didSet {
            let locationPoint = self.mapView.convert(pinPoint!, toCoordinateFrom: self.mapView)
            
            self.pinCoordinate = locationPoint
        }
    }
    
    public var pinCoordinate: CLLocationCoordinate2D? {
        didSet {
            guard let coordinate = pinCoordinate else { return }
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotation(annotation)
            
            self.zoomMap(coordinate: annotation.coordinate)
        }
    }
    
    private func zoomMap(coordinate: CLLocationCoordinate2D) {
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegionMake(coordinate, span)
        self.mapView.setRegion(region, animated: true)
        
        self.searchBar.resignFirstResponder()
        
    }
}
