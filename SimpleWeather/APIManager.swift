//
//  APIManager.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit

class APIManager: NSObject {

    static let apiKey = "c6e381d8c7ff98f0fee43775817cf6ad"
    static let serverURL = "http://api.openweathermap.org/data/2.5/"
    
    internal enum HTTPSendMethod: String {
        case put = "PUT"
        case post = "POST"
        case get = "GET"
        case delete = "DELETE"
    }
    
    private class func send(method: HTTPSendMethod, urlRequest: URL, completion:@escaping (_ response: [String: Any]?, _ error: Error?) -> Void) {
        var request = URLRequest(url: urlRequest)
        request.httpMethod = method.rawValue
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error != nil {
                DispatchQueue.main.async {
                    completion(nil, error)
                    
                    return
                }
            }
            
            if let responseData = data {
                do {
                    
                    let resultDictionary = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    
                    if resultDictionary != nil {
                        DispatchQueue.main.async {
                            completion(resultDictionary, nil)
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
        task.resume()
    }
    
    class func todayForecast(latitude: Float, longitude: Float, completion:@escaping (_ response: [String: Any]?, _ error: Error?) -> Void) {
        let unit: String?
        
        if let unitSystem = UserDefaults.standard.object(forKey: "unit-system") as? String {
            unit = unitSystem
        } else {
            unit = "metric"
        }
        
        let todayForecast = "weather?lat=\(latitude)&lon=\(longitude)&appid=\(apiKey)&units=\(unit!)"
        let todayForecastURL = URL(string:serverURL + todayForecast)
        
        send(method: .get, urlRequest: todayForecastURL!, completion: {
            response, error in
            
            completion(response, error)
        })
    }
    
    class func fiveDayForecast(latitude: Float, longitude: Float, completion:@escaping (_ response: [String: Any]?, _ error: Error?) -> Void) {
        let unit: String?
        
        if let unitSystem = UserDefaults.standard.object(forKey: "unit-system") as? String {
            unit = unitSystem
        } else {
            unit = "metric"
        }
        
        let todayForecast = "forecast?lat=\(latitude)&lon=\(longitude)&appid=\(apiKey)&units=\(unit!)"
        let todayForecastURL = URL(string:serverURL + todayForecast)
        
        send(method: .get, urlRequest: todayForecastURL!, completion: {
            response, error in
            
            completion(response, error)
        })
    }
}
