//
//  AddCityViewController.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit
import MapKit

public protocol AddCityViewControllerDelegate: class {
    func doneButtonWasPressed(data: CLPlacemark)
}

class AddCityViewController: UIViewController {

    internal weak var delegate: AddCityViewControllerDelegate?
    
    internal var cityItems: [MKMapItem] = []
    internal var previousSearch: MKLocalSearch?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(_:)))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed(_:)))
        
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = doneButton
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        let longGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longGestureRecognizerAction(_:)))
        longGestureRecognizer.minimumPressDuration = 0.5
        
        guard let addCityView = self.view as? AddCityView else { return }
        addCityView.mapView.addGestureRecognizer(longGestureRecognizer)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    func doneButtonPressed(_ sender: UIBarButtonItem) {
        guard let addCityView = self.view as? AddCityView else { return }
        guard let coordinate = addCityView.pinCoordinate else { return }
        
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in
            
            if error != nil {
                print(error?.localizedDescription ?? "Some error ocures")
                return
            }
            
            guard let placemark = placemarks?.first else { return }
            self.delegate?.doneButtonWasPressed(data: placemark)
            
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func cancelButtonPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func longGestureRecognizerAction(_ sender: UIGestureRecognizer) {
        if sender.state != .began { return }
        
        guard let addCityView = self.view as? AddCityView else { return }
        
        let touchPoint = sender.location(in: addCityView.mapView)
        addCityView.pinPoint = touchPoint
        
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    // MARK: - MKMapItem Array Functions
    
    func createCityArray(from: [MKMapItem]) -> [MKMapItem] {
        var citiesSearchResult: [MKMapItem] = []
        
        for city in from {
            if city.placemark.locality != nil && checkIf(city: city, existInList: citiesSearchResult) {
                citiesSearchResult.append(city)
            }
        }
        
        return citiesSearchResult
    }
    
    func checkIf(city: MKMapItem, existInList: [MKMapItem]) -> Bool {
        for listObject in existInList {
            if listObject.placemark.locality == city.placemark.locality &&
                listObject.placemark.administrativeArea == city.placemark.administrativeArea &&
                listObject.placemark.country == city.placemark.country {
                return false
            }
        }
        
        return true
    }
}

extension AddCityViewController: UISearchBarDelegate {
    // MARK: - Search bar data source
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchText
        self.previousSearch?.cancel()
        
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: {
            response, error in
            
            guard let response = response else { return }
            guard let addCityView = self.view as? AddCityView else { return }
            
            self.cityItems = self.createCityArray(from: response.mapItems)
            addCityView.tableView.reloadData()
        })
        
        self.previousSearch = search
    }
}

extension AddCityViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath)
        let cityData = cityItems[indexPath.row]
        
        cell.textLabel?.text = cityData.placemark.locality
        cell.detailTextLabel?.text = (cityData.placemark.administrativeArea ?? "") + ", " + (cityData.placemark.country ?? "")
        
        return cell
    }
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedPlacemark = cityItems[indexPath.row].placemark
        
        guard let addCityView = self.view as? AddCityView else { return }
        addCityView.pinCoordinate = selectedPlacemark.coordinate
        
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
}
