//
//  SettingsViewController.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 02.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import UIKit

public protocol SettingsViewControllerDelegate: class {
    func settingsDismissed()
}

class SettingsViewController: UIViewController {

    internal weak var delegate: SettingsViewControllerDelegate?
    
    @IBOutlet weak var helpWebView: UIWebView!
    @IBOutlet weak var unitSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Prepare User Interface
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(_:)))
        self.navigationItem.rightBarButtonItem = doneButton
        
        if let unitSystem = UserDefaults.standard.object(forKey: "unit-system") as? String {
            switch unitSystem {
            case "metric":
                self.unitSegmentedControl.selectedSegmentIndex = 0
            case "imperial":
                self.unitSegmentedControl.selectedSegmentIndex = 1
            default:
                self.unitSegmentedControl.selectedSegmentIndex = 0
            }
        } else {
            self.unitSegmentedControl.selectedSegmentIndex = 0
        }
        
        //Load help
        //let htmlFile = Bundle.main.path(forResource: "help", ofType: "html")
        //let html = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
        //helpWebView.loadHTMLString(html!, baseURL: nil)
        
        let webpageURL = URL(string: "http://jeremicz.com")
        helpWebView.loadRequest(URLRequest(url: webpageURL!))
    }

    @IBAction func unitSystemAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            UserDefaults.standard.set("metric", forKey: "unit-system")
        case 1:
            UserDefaults.standard.set("imperial", forKey: "unit-system")
        default:
            UserDefaults.standard.set("metric", forKey: "unit-system")
        }
        UserDefaults.standard.synchronize()
    }
    
    func doneButtonPressed(_ sender: UIBarButtonItem) {
        self.delegate?.settingsDismissed()
        self.dismiss(animated: true, completion: nil)
    }

}
