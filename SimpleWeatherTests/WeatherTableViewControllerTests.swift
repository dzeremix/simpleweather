//
//  WeatherTableViewControllerTests.swift
//  SimpleWeather
//
//  Created by Piotrek Jeremicz on 03.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import XCTest
@testable import SimpleWeather

class WeatherTableViewControllerTests: XCTestCase {
    
    var testWeatherTableView: WeatherTableViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        self.testWeatherTableView = storyboard.instantiateViewController(withIdentifier: "weatherTableViewCotroller") as! WeatherTableViewController
        self.testWeatherTableView.viewDidLoad()
    }
    
    override func tearDown() {
        testWeatherTableView = nil
        super.tearDown()
    }
    
    func testThatViewLoads() {
        XCTAssertNotNil(self.testWeatherTableView.view, "View not initiated properly")
    }
    
    func testThatTableViewLoads() {
        XCTAssertNotNil(self.testWeatherTableView.tableView, "TableView not initiated")
    }
    
    func testThatViewConformsToUITableViewDataSource() {
        XCTAssertTrue(self.testWeatherTableView.conforms(to: UITableViewDataSource.self), "View does not conform to UITableView datasource protocol")
    }
    
    func testThatTableViewHasDataSource() {
        XCTAssertNotNil(self.testWeatherTableView.tableView.dataSource, "Table datasource cannot be nil")
    }
    
    func testThatViewConformsToUITableViewDelegate() {
        XCTAssertTrue(self.testWeatherTableView.conforms(to: UITableViewDelegate.self), "View does not conform to UITableView delegate protocol")
    }
    
    func testTableViewIsConnectedToDelegate() {
        XCTAssertNotNil(self.testWeatherTableView.tableView.delegate, "Table datasource cannot be nil")
    }
    
    
    func testTableViewHeightForRowAtIndexPath() {
        let expectedHeight: CGFloat = 80.0
        let actualHeight = self.testWeatherTableView.tableView.rowHeight
        XCTAssertEqual(expectedHeight, actualHeight, "Cell should have \(expectedHeight) height, but they have \(actualHeight)")
    }
    
    func testTableViewCellCreateCellsWithReuseIdentifier() {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.testWeatherTableView.tableView(self.testWeatherTableView.tableView, cellForRowAt: indexPath)
        let string = "cityCell"
        XCTAssertTrue(cell.reuseIdentifier == string, "Table does not create reusable cells");
    }
}
