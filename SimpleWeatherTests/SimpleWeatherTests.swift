//
//  SimpleWeatherTests.swift
//  SimpleWeatherTests
//
//  Created by Piotrek Jeremicz on 01.07.2017.
//  Copyright © 2017 Piotrek Jeremicz. All rights reserved.
//

import MapKit
import XCTest
@testable import SimpleWeather

class SimpleWeatherTests: XCTestCase {
    
    var testAddCityViewController: AddCityViewController!
    
    override func setUp() {
        super.setUp()

        testAddCityViewController = AddCityViewController()
    }
    
    override func tearDown() {
        testAddCityViewController = nil
        super.tearDown()
    }
    
    func testTodayForecast() {
        APIManager.todayForecast(latitude: 0.0, longitude: 0.0, completion: {
            error, response in
            
            XCTAssertNotNil(error, "Today Forecast Error Response")
            XCTAssertNil(response, "Today Forecast Response Nil")
        })
    }
    
    func testFiveDayForecastForecast() {
        APIManager.fiveDayForecast(latitude: 0.0, longitude: 0.0, completion: {
            error, response in
            
            XCTAssertNotNil(error, "Five Day Forecast Error Response")
            XCTAssertNil(response, "Five Day Forecast Response Nil")
        })
    }
    
    func testCheckCityExist() {
        testAddCityViewController.cityItems = [MKMapItem(), MKMapItem()]
        let check = testAddCityViewController.checkIf(city: MKMapItem(), existInList: testAddCityViewController.cityItems)
        
        XCTAssertEqual(check, false, "Function doesn't recognize the same city")
    }
}
